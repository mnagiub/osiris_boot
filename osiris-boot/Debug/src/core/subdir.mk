################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/core/boot.c \
../src/core/mqueue.c \
../src/core/mutex.c \
../src/core/sleep.c \
../src/core/wqueue.c 

C_DEPS += \
./src/core/boot.d \
./src/core/mqueue.d \
./src/core/mutex.d \
./src/core/sleep.d \
./src/core/wqueue.d 

OBJS += \
./src/core/boot.o \
./src/core/mqueue.o \
./src/core/mutex.o \
./src/core/sleep.o \
./src/core/wqueue.o 


# Each subdirectory must supply rules for building sources it contributes
src/core/%.o: ../src/core/%.c src/core/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -DMACHINE_NAME=native_linux -I"/home/mena/osiris_boot/osiris-boot/src" -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


clean: clean-src-2f-core

clean-src-2f-core:
	-$(RM) ./src/core/boot.d ./src/core/boot.o ./src/core/mqueue.d ./src/core/mqueue.o ./src/core/mutex.d ./src/core/mutex.o ./src/core/sleep.d ./src/core/sleep.o ./src/core/wqueue.d ./src/core/wqueue.o

.PHONY: clean-src-2f-core

