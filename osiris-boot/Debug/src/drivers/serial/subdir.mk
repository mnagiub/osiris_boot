################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/drivers/serial/serial.c 

C_DEPS += \
./src/drivers/serial/serial.d 

OBJS += \
./src/drivers/serial/serial.o 


# Each subdirectory must supply rules for building sources it contributes
src/drivers/serial/%.o: ../src/drivers/serial/%.c src/drivers/serial/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -DMACHINE_NAME=native_linux -I"/home/mena/osiris_boot/osiris-boot/src" -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


clean: clean-src-2f-drivers-2f-serial

clean-src-2f-drivers-2f-serial:
	-$(RM) ./src/drivers/serial/serial.d ./src/drivers/serial/serial.o

.PHONY: clean-src-2f-drivers-2f-serial

