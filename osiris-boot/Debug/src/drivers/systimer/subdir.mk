################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/drivers/systimer/systimer.c 

C_DEPS += \
./src/drivers/systimer/systimer.d 

OBJS += \
./src/drivers/systimer/systimer.o 


# Each subdirectory must supply rules for building sources it contributes
src/drivers/systimer/%.o: ../src/drivers/systimer/%.c src/drivers/systimer/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -DMACHINE_NAME=native_linux -I"/home/mena/osiris_boot/osiris-boot/src" -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


clean: clean-src-2f-drivers-2f-systimer

clean-src-2f-drivers-2f-systimer:
	-$(RM) ./src/drivers/systimer/systimer.d ./src/drivers/systimer/systimer.o

.PHONY: clean-src-2f-drivers-2f-systimer

