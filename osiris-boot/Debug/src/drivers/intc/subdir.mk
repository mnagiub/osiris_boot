################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/drivers/intc/intc.c 

C_DEPS += \
./src/drivers/intc/intc.d 

OBJS += \
./src/drivers/intc/intc.o 


# Each subdirectory must supply rules for building sources it contributes
src/drivers/intc/%.o: ../src/drivers/intc/%.c src/drivers/intc/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -DMACHINE_NAME=native_linux -I"/home/mena/osiris_boot/osiris-boot/src" -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


clean: clean-src-2f-drivers-2f-intc

clean-src-2f-drivers-2f-intc:
	-$(RM) ./src/drivers/intc/intc.d ./src/drivers/intc/intc.o

.PHONY: clean-src-2f-drivers-2f-intc

