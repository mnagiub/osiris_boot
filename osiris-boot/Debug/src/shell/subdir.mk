################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/shell/shell_framework.c 

C_DEPS += \
./src/shell/shell_framework.d 

OBJS += \
./src/shell/shell_framework.o 


# Each subdirectory must supply rules for building sources it contributes
src/shell/%.o: ../src/shell/%.c src/shell/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -DMACHINE_NAME=native_linux -I"/home/mena/osiris_boot/osiris-boot/src" -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


clean: clean-src-2f-shell

clean-src-2f-shell:
	-$(RM) ./src/shell/shell_framework.d ./src/shell/shell_framework.o

.PHONY: clean-src-2f-shell

