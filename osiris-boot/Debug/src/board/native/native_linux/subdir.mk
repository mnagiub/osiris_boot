################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/board/native/native_linux/native_linux_board.c 

C_DEPS += \
./src/board/native/native_linux/native_linux_board.d 

OBJS += \
./src/board/native/native_linux/native_linux_board.o 


# Each subdirectory must supply rules for building sources it contributes
src/board/native/native_linux/%.o: ../src/board/native/native_linux/%.c src/board/native/native_linux/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -DMACHINE_NAME=native_linux -I"/home/mena/osiris_boot/osiris-boot/src" -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


clean: clean-src-2f-board-2f-native-2f-native_linux

clean-src-2f-board-2f-native-2f-native_linux:
	-$(RM) ./src/board/native/native_linux/native_linux_board.d ./src/board/native/native_linux/native_linux_board.o

.PHONY: clean-src-2f-board-2f-native-2f-native_linux

