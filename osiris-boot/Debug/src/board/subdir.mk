################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/board/board.c 

C_DEPS += \
./src/board/board.d 

OBJS += \
./src/board/board.o 


# Each subdirectory must supply rules for building sources it contributes
src/board/%.o: ../src/board/%.c src/board/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -DMACHINE_NAME=native_linux -I"/home/mena/osiris_boot/osiris-boot/src" -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


clean: clean-src-2f-board

clean-src-2f-board:
	-$(RM) ./src/board/board.d ./src/board/board.o

.PHONY: clean-src-2f-board

