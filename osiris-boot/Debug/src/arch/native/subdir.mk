################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/arch/native/Start.c \
../src/arch/native/arch.c \
../src/arch/native/cpu.c \
../src/arch/native/mem.c \
../src/arch/native/soc.c 

C_DEPS += \
./src/arch/native/Start.d \
./src/arch/native/arch.d \
./src/arch/native/cpu.d \
./src/arch/native/mem.d \
./src/arch/native/soc.d 

OBJS += \
./src/arch/native/Start.o \
./src/arch/native/arch.o \
./src/arch/native/cpu.o \
./src/arch/native/mem.o \
./src/arch/native/soc.o 


# Each subdirectory must supply rules for building sources it contributes
src/arch/native/%.o: ../src/arch/native/%.c src/arch/native/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -DMACHINE_NAME=native_linux -I"/home/mena/osiris_boot/osiris-boot/src" -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


clean: clean-src-2f-arch-2f-native

clean-src-2f-arch-2f-native:
	-$(RM) ./src/arch/native/Start.d ./src/arch/native/Start.o ./src/arch/native/arch.d ./src/arch/native/arch.o ./src/arch/native/cpu.d ./src/arch/native/cpu.o ./src/arch/native/mem.d ./src/arch/native/mem.o ./src/arch/native/soc.d ./src/arch/native/soc.o

.PHONY: clean-src-2f-arch-2f-native

