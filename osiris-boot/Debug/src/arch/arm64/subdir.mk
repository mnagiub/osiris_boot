################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/arch/arm64/Start.c \
../src/arch/arm64/arch.c \
../src/arch/arm64/cpu.c \
../src/arch/arm64/mem.c \
../src/arch/arm64/soc.c 

C_DEPS += \
./src/arch/arm64/Start.d \
./src/arch/arm64/arch.d \
./src/arch/arm64/cpu.d \
./src/arch/arm64/mem.d \
./src/arch/arm64/soc.d 

OBJS += \
./src/arch/arm64/Start.o \
./src/arch/arm64/arch.o \
./src/arch/arm64/cpu.o \
./src/arch/arm64/mem.o \
./src/arch/arm64/soc.o 


# Each subdirectory must supply rules for building sources it contributes
src/arch/arm64/%.o: ../src/arch/arm64/%.c src/arch/arm64/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -DMACHINE_NAME=native_linux -I"/home/mena/osiris_boot/osiris-boot/src" -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


clean: clean-src-2f-arch-2f-arm64

clean-src-2f-arch-2f-arm64:
	-$(RM) ./src/arch/arm64/Start.d ./src/arch/arm64/Start.o ./src/arch/arm64/arch.d ./src/arch/arm64/arch.o ./src/arch/arm64/cpu.d ./src/arch/arm64/cpu.o ./src/arch/arm64/mem.d ./src/arch/arm64/mem.o ./src/arch/arm64/soc.d ./src/arch/arm64/soc.o

.PHONY: clean-src-2f-arch-2f-arm64

