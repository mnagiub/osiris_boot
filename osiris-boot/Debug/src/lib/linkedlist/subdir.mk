################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/lib/linkedlist/linkedlist.c 

C_DEPS += \
./src/lib/linkedlist/linkedlist.d 

OBJS += \
./src/lib/linkedlist/linkedlist.o 


# Each subdirectory must supply rules for building sources it contributes
src/lib/linkedlist/%.o: ../src/lib/linkedlist/%.c src/lib/linkedlist/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -DMACHINE_NAME=native_linux -I"/home/mena/osiris_boot/osiris-boot/src" -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


clean: clean-src-2f-lib-2f-linkedlist

clean-src-2f-lib-2f-linkedlist:
	-$(RM) ./src/lib/linkedlist/linkedlist.d ./src/lib/linkedlist/linkedlist.o

.PHONY: clean-src-2f-lib-2f-linkedlist

