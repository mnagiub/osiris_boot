################################################################################
# Automatically-generated file. Do not edit!
################################################################################

ASM_SRCS := 
C_SRCS := 
OBJ_SRCS := 
O_SRCS := 
S_UPPER_SRCS := 
C_DEPS := 
EXECUTABLES := 
OBJS := 

# Every subdirectory with source files must be described here
SUBDIRS := \
src/arch/arm64 \
src/arch/native \
src/board \
src/board/native_linux \
src/bootapp \
src/cmd/cmd_framework \
src/cmd/cmd_parser \
src/compiler/string \
src/core \
src/drivers \
src/drivers/gpio \
src/drivers/intc \
src/drivers/serial/linux_console \
src/drivers/serial \
src/drivers/systimer/linux_systimer \
src/drivers/systimer \
src/lib/linkedlist \
src \
src/shell \

