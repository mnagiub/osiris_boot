/*
 * bootapp_main.c
 *
 *  Created on: Jan 23, 2022
 *      Author: mena
 */

#include <configs/config.h>
#include <include/bootapp/bootapp.h>

#ifdef CONFIG_BOOT_APP
#if (CONFIG_BOOT_APP == 'Y')

int bootapp_main(int argc, char* argv[]) {
	return 0;
}

#endif
#endif
