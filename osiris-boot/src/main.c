/*
 * main.c
 *
 *  Created on: Jan 13, 2022
 *      Author: mena
 */

#include <configs/config.h>
#include <include/board/board.h>
#include <include/boot.h>

int main(void) {

	bootloader_init(0, 0);

	bootloader_main_loop(0, 0);

	return 0;
}



