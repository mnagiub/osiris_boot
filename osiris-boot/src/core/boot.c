/*
 * boot.c
 *
 *  Created on: Jan 14, 2022
 *      Author: mena
 */

#include <configs/config.h>
#include <include/boot.h>
#include <include/error.h>
#include <include/shell/shell.h>
#include <include/board/board.h>
#include <include/arch/arch.h>
#include <include/boot/mqueue.h>
#include <include/boot/mutex.h>
#include <include/boot/sleep.h>
#include <include/boot/wqueue.h>
#include <include/bootapp/bootapp.h>

#ifdef CONFIG_WAIT_QUEUE
#if (CONFIG_WAIT_QUEUE == 'Y')
#define SERAIL_INPUT_EVT		0x01
#define BOOTAPP_PROCESS_EVT		0x02
#endif
#endif

#ifdef CONFIG_WAIT_QUEUE
#if (CONFIG_WAIT_QUEUE == 'Y')
static int process_events(u64 evt_flags, u32* const opt_flags) {
	return 0;
}
#endif
#endif

#ifdef CONFIG_DISPLAY_BANNER
#if (CONFIG_DISPLAY_BANNER == 'Y')
static int bootloader_print_banner(void) {
	return 0;
}
#endif
#endif

int bootloader_init(const u32 mode, const u32 flags) {

	cpu_init(0, 0);
	mem_init(0, 0);
	soc_init(0, 0);

	preempt_disable();

	platform_initialize();

#ifdef CONFIG_WAIT_QUEUE
#if (CONFIG_WAIT_QUEUE == 'Y')
	waitq_init();
#endif
#endif

#ifdef CONFIG_MESSAGE_QUEUE
#if (CONFIG_MESSAGE_QUEUE == 'Y')
	msgq_init();
#endif
#endif

#ifdef CONFIG_MUTEX
#if (CONFIG_MUTEX == 'Y')
	mutex_init();
#endif
#endif

#ifdef CONFIG_SLEEP_FUNCS
#if (CONFIG_SLEEP_FUNCS == 'Y')
	delay_calibrate(DELAY_CALIB_DURATION_MS);
#endif
#endif

	shell_init(0, 0);

	return EOK;
}

int bootloader_main_loop(const u32 mode, const u32 flags) {

	u32 loop_flags = 1;
#ifdef CONFIG_WAIT_QUEUE
#if (CONFIG_WAIT_QUEUE == 'Y')
	u64 evt_flags;
	u8 wqid = 0;
#endif
#endif

#ifdef CONFIG_BOOT_APP
#if (CONFIG_BOOT_APP == 'Y')
	int bootapp_argc;
	char** bootapp_argv;
#endif
#endif

	shell_start(0, 0);

	preempt_enable();

#ifdef CONFIG_DISPLAY_BANNER
#if (CONFIG_DISPLAY_BANNER == 'Y')
	bootloader_print_banner();
#endif
#endif

	do {

#ifdef CONFIG_WAIT_QUEUE
#if (CONFIG_WAIT_QUEUE == 'Y')
		waitq_wait_for_event(wqid, &evt_flags, 1);
		process_events(evt_flags, &loop_flags);
#endif
#endif

#ifdef CONFIG_WAIT_QUEUE
#if (CONFIG_WAIT_QUEUE == 'Y')
		if ((loop_flags & SERAIL_INPUT_EVT) == SERAIL_INPUT_EVT) {
			shell_process_serial_input(&evt_flags);
			if ((evt_flags & RETURN_CHAR_RCV_EVT) == RETURN_CHAR_RCV_EVT) {
#endif
#endif
				shell_parse_commands();
				shell_handle_commands();
				shell_respond();
				shell_loop_finalize();

				printf("\n Shell output");

#ifdef CONFIG_WAIT_QUEUE
#if (CONFIG_WAIT_QUEUE == 'Y')
			}
		}
#endif
#endif

#ifdef CONFIG_WAIT_QUEUE
#if (CONFIG_WAIT_QUEUE == 'Y')
		waitq_wait_for_event(wqid, &evt_flags, 1);

		process_events(evt_flags, &loop_flags);

		if ((loop_flags & BOOTAPP_PROCESS_EVT) == BOOTAPP_PROCESS_EVT) {
#endif
#endif

#ifdef CONFIG_BOOT_APP
#if (CONFIG_BOOT_APP == 'Y')
			bootapp_main(bootapp_argc, bootapp_argv);
#endif
#endif

#ifdef CONFIG_WAIT_QUEUE
#if (CONFIG_WAIT_QUEUE == 'Y')
		}
#endif
#endif

	}while(0 != loop_flags);

	return EOK;

}
