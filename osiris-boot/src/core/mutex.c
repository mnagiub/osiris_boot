/*
 * spinlock.c
 *
 *  Created on: Jan 22, 2022
 *      Author: mena
 */

#include <configs/config.h>
#include <include/boot/mutex.h>
#include <include/common.h>
#include <include/error.h>
#include <include/compiler/stdlib.h>
#include <include/arch/arch.h>


#ifdef CONFIG_MUTEX
#if (CONFIG_MUTEX == 'Y')

int mutex_init(void) {
	return EOK;
}

int mutex_acquire(const u8 mtxid) {
	return EOK;
}

int mutex_release(const u8 mtxid) {
	return EOK;
}

int mutex_acquire_nonsleep(const u8 mtxid) {
	return EOK;
}

int mutex_try_acquire(const u8 mtxid) {
	return EOK;
}

int mutex_release_from_irq(const u8 mtxid) {
	return EOK;
}

#endif
#endif
