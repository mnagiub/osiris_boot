/*
 * message.c
 *
 *  Created on: Jan 22, 2022
 *      Author: mena
 */

#include <configs/config.h>
#include <include/boot/mqueue.h>
#include <include/common.h>
#include <include/error.h>
#include <include/compiler/stdlib.h>

#ifdef CONFIG_MESSAGE_QUEUE
#if (CONFIG_MESSAGE_QUEUE == 'Y')
int msgq_init(void) {
	return EOK;
}

int msgq_push_msg(const u8 mqid, const u8* const buf, size_t len) {
	return EOK;
}

int msgq_pop_msg(const u8 mqid, u8* const buf, size_t len) {
	return EOK;
}

int msgq_enqueue_msg(const u8 mqid, const u8* const buf, size_t len) {
	return EOK;
}

int msgq_dequeue_msg(const u8 mqid, u8* const buf, size_t len) {
	return EOK;
}
#endif
#endif

