/*
 * event.c
 *
 *  Created on: Jan 14, 2022
 *      Author: mena
 */

#include <configs/config.h>
#include <include/boot/wqueue.h>
#include <include/common.h>
#include <include/error.h>
#include <include/compiler/stdlib.h>

#ifdef CONFIG_WAIT_QUEUE
#if (CONFIG_WAIT_QUEUE == 'Y')
int waitq_init(void) {
	return EOK;
}

int waitq_wait_for_event(const u8 wqid, u64* const evt_flags, const u8 clear) {
	return EOK;
}

int waitq_wait_for_event_in_irq(const u8 wqid, u64* const evt_flags, const u8 clear) {
	return EOK;
}

int waitq_send_event(const u8 wqid, const u64 evt_flags) {
	return EOK;
}

int waitq_send_event_from_irq(const u8 wqid, const u64 evt_flags) {
	return EOK;
}
#endif
#endif
