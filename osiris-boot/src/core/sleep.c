/*
 * sleep.c
 *
 *  Created on: Jan 14, 2022
 *      Author: mena
 */

#include <configs/config.h>
#include <include/boot/sleep.h>
#include <include/common.h>
#include <include/error.h>
#include <include/compiler/stdlib.h>
#include <include/arch/arch.h>

#ifdef CONFIG_SLEEP_FUNCS
#if (CONFIG_SLEEP_FUNCS == 'Y')
int delay_calibrate(u32 calibration_period_ms) {
	return EOK;
}

#ifdef CONFIG_SEC_SLEEP
#if (CONFIG_SEC_SLEEP == 'Y')
int delay_sec(u32 sec) {
	return EOK;
}
#endif
#endif

#ifdef CONFIG_MS_SLEEP
#if (CONFIG_MS_SLEEP == 'Y')
int delay_msec(u32 msec) {
	return EOK;
}
#endif
#endif

#ifdef CONFIG_US_SLEEP
#if (CONFIG_US_SLEEP == 'Y')
int delay_usec(u32 usec){
	return EOK;
}
#endif
#endif

int sleep_for_interrupt(u32 const* flags) {
	return cpu_wait_for_interrupt(flags);
}
#endif
#endif
