/*
 * mem.c
 *
 *  Created on: Jan 14, 2022
 *      Author: mena
 */


#include <configs/config.h>
#include <include/common.h>
#include <include/error.h>
#include <include/arch/arch.h>

#if defined(ARCH) && (ARCH_ARM64 == 'Y')

int mem_init(const u32 mode, const u32 flags) {
	return EOK;
}

#endif
