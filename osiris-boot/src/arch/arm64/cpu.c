/*
 * cpu.c
 *
 *  Created on: Jan 14, 2022
 *      Author: mena
 */

#include <configs/config.h>
#include <include/common.h>
#include <include/error.h>
#include <include/arch/arch.h>

#if defined(ARCH) && (ARCH_ARM64 == 'Y')

int cpu_init(const u32 mode, const u32 flags) {
	return EOK;
}

int cpu_wait_for_interrupt(u32 const* flags) {
	return EOK;
}

int print_cpu_context(const u8 cpuid, char* buf, u16 const* len) {
	return EOK;
}

#endif


