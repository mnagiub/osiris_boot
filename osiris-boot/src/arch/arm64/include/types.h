/*
 * types.h
 *
 *  Created on: Jan 13, 2022
 *      Author: mena
 */

#ifndef SRC_ARCH_ARM64_TYPES_H_
#define SRC_ARCH_ARM64_TYPES_H_

#include <configs/config.h>

#if defined(ARCH) && (ARCH_ARM64 == 'Y')
typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned long u32;
typedef unsigned long long u64;

typedef signed char s8;
typedef signed short s16;
typedef signed long s32;
typedef signed long long s64;
#endif

#endif /* SRC_ARCH_ARM64_TYPES_H_ */
