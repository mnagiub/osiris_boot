/*
 * arch.c
 *
 *  Created on: Jan 14, 2022
 *      Author: mena
 */


#include <configs/config.h>
#include <include/common.h>
#include <include/error.h>
#include <include/arch/arch.h>

#if defined(ARCH) && (ARCH_ARM64 == 'Y')

int preempt_disable(void) {
	return EOK;
}

int preempt_enable(void) {
	return EOK;
}

int preempt_get_status(u8 const* status) {
	return EOK;
}

#endif
