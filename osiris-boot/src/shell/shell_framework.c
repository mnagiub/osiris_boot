/*
 * shell_framework.c
 *
 *  Created on: Jan 14, 2022
 *      Author: mena
 */


#include <configs/config.h>
#include <include/common.h>
#include <include/error.h>

int shell_init(const u32 mode, u32 flags) {

	return EOK;
}

int shell_start(const u32 mode, u32 flags) {

	return EOK;
}

int shell_process_serial_input(u64* const events) {

	return EOK;
}

int shell_parse_commands(void) {

	return EOK;
}

int shell_handle_commands(void) {

	return EOK;
}

int shell_respond(void) {

	return EOK;
}

int shell_loop_finalize(void) {

	return EOK;
}
