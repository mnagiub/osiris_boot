/*
 * boot.h
 *
 *  Created on: Jan 13, 2022
 *      Author: mena
 */

#ifndef SRC_INCLUDE_BOOT_H_
#define SRC_INCLUDE_BOOT_H_

#include <configs/config.h>

extern int bootloader_init(const u32 mode, const u32 flags);
extern int bootloader_main_loop(const u32 mode, const u32 flags);

#endif /* SRC_INCLUDE_BOOT_H_ */
