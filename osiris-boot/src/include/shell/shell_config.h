/*
 * shell_config.h
 *
 *  Created on: Jan 14, 2022
 *      Author: mena
 */

#ifndef SRC_INCLUDE_SHELL_SHELL_CONFIG_H_
#define SRC_INCLUDE_SHELL_SHELL_CONFIG_H_

#define SHELL_PROMPT 		"$"
#define SHELL_BUFFER_LEN	128

#endif /* SRC_INCLUDE_SHELL_SHELL_CONFIG_H_ */
