/*
 * shell.h
 *
 *  Created on: Jan 14, 2022
 *      Author: mena
 */

#ifndef SRC_INCLUDE_SHELL_SHELL_H_
#define SRC_INCLUDE_SHELL_SHELL_H_

#include <configs/config.h>
#include <include/common.h>
#include <include/error.h>

#define RETURN_CHAR_RCV_EVT		0x01
#define STOP_CHAR_RCV_EVT		0x02
#define CANCEL_CHAR_RCV_EVT		0x04

extern int shell_init(const u32 mode, u32 flags);
extern int shell_start(const u32 mode, u32 flags);
extern int shell_process_serial_input(u64* const events);
extern int shell_parse_commands(void);
extern int shell_handle_commands(void);
extern int shell_respond(void);
extern int shell_loop_finalize(void);

#endif /* SRC_INCLUDE_SHELL_SHELL_H_ */
