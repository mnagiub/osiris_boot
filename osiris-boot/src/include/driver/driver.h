/*
 * driver.h
 *
 *  Created on: Jan 22, 2022
 *      Author: mena
 */

#ifndef SRC_INCLUDE_DRIVER_DRIVER_H_
#define SRC_INCLUDE_DRIVER_DRIVER_H_

#include <configs/config.h>

struct driver_struct {
	char* dev_name;
	char* cls_name;
	int class_id;
	int dev_num;
	int devhndl;
	void* pvdata;
};

extern int driver_register_device_class(const char* const cls_name, const u8 dev_cnt, u8* const pchndl);
extern int driver_register_device(struct driver_struct** pdev, const u8 chndl);
extern int driver_unregister_device(struct driver_struct* const pdev);
extern int driver_unregister_device_class(const char* const class_name);
extern int driver_open_device(const char* const dev_name, struct driver_struct** ppdev);

#endif /* SRC_INCLUDE_DRIVER_DRIVER_H_ */
