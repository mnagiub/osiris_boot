/*
 * serial.h
 *
 *  Created on: Jan 13, 2022
 *      Author: mena
 */

#ifndef SRC_DRIVERS_INCLUDE_SERIAL_H_
#define SRC_DRIVERS_INCLUDE_SERIAL_H_

#include <configs/config.h>
#include <include/driver/driver.h>
#include <include/compiler/stdlib.h>

struct serial_driver {
	struct driver_struct* pvdrv;
	int (*serial_init)(struct serial_driver* pdev);
	int (*serial_open)(struct serial_driver* pdev, const u32 mode, const u64 flags);
	int (*serial_read)(struct serial_driver* pdev, char* const buf, size_t* const len);
	int (*serial_write)(struct serial_driver* pdev, const char* const buf, const size_t len);
	int (*serial_ioctl)(struct serial_driver* pdev, const u32 mode, const u64 flags, u64* const pdata);
	int (*serial_poll)(struct serial_driver* pdev, const u32 timeout);
	int (*serial_close)(struct serial_driver* pdev);
	int (*serial_deinit)(struct serial_driver* pdev);
	void* pdata;
};

extern int serial_platform_create(const u8 dev_cnt);
extern int serial_device_create(struct serial_driver* pdev);
extern int serial_open(const char* const dev_name, struct serial_driver** ppdev);
extern int serial_read(struct serial_driver* pdev, char* const buf, size_t* const len);
extern int serial_write(struct serial_driver* pdev, const char* const buf, const size_t len);
extern int serial_ioctl(struct serial_driver* pdev, const u32 mode, const u64 flags, u64* const pdata);
extern int serial_poll(struct serial_driver* pdev, const u32 timeout);
extern int serial_close(struct serial_driver* pdev);

#endif /* SRC_DRIVERS_INCLUDE_SERIAL_H_ */
