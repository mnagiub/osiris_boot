/*
 * timer.h
 *
 *  Created on: Jan 13, 2022
 *      Author: mena
 */

#ifndef SRC_DRIVERS_INCLUDE_SYSTIMER_H_
#define SRC_DRIVERS_INCLUDE_SYSTIMER_H_

#include <configs/config.h>
#include "driver.h"

struct systimer_driver {
	struct driver_struct* pvdrv;
	int (*systimer_init)(struct systimer_driver* pdev);
	int (*systimer_deinit)(struct systimer_driver* pdev);
	int (*systimer_ioctl)(struct systimer_driver* pdev, const u32 mode, const u64 flags, u64* const pdata);
	int (*systimer_read)(struct systimer_driver* pdev, u64* const pdata);
	int (*callback)(void* data);
	void* callback_data;
	void* pdata;
	u32 s_interval_us;
	u32 period_us;
};

struct systimer_conf_struct {
	int (*callback)(void* data);
	void* callback_data;
	u32 s_interval_us;
	u32 period_us;
};

#ifndef SYS_CLK_FREQ_HZ
#define SYS_CLK_FREQ_HZ	100
#endif

#define SYSTIMER_MS_TO_HZ(ms)	((ms * SYS_CLK_FREQ_HZ) / 1000)
#define SYSTIMER_US_TO_HZ(us)	((us * SYS_CLK_FREQ_HZ) / 1000000)
#define SYSTIMER_SEC_TO_HZ(sec)	(sec * SYS_CLK_FREQ_HZ)

#define SYSTIMER_IOCTL_TIMER_MODE		1
#define SYSTIMER_IOCTL_FRC_MODE			2
#define SYSTIMER_IOCTL_TIMER_START		3
#define SYSTIMER_IOCTL_TIMER_STOP		4
#define SYSTIMER_IOCTL_FRC_START		5
#define SYSTIMER_IOCTL_FRC_STOP			6

extern int systimer_platform_create(const u8 dev_cnt);
extern int systimer_device_create(struct systimer_driver* pdev);

extern int systimer_open(const char* const dev_name, struct systimer_driver** ppdev);
extern int systimer_start_timer(struct systimer_driver* pdev, const struct systimer_conf_struct conf, const u32 mode);

extern int systimer_stop_timer(struct systimer_driver* dev);

extern int systimer_start_frc(struct systimer_driver* pdev, const u32 mode, const u64 flags);
extern int systimer_stop_frc(struct systimer_driver* pdev);
extern int systimer_read_frc(struct systimer_driver* pdev, u64* const val);
extern int systimer_close(struct systimer_driver* pdev);

#endif /* SRC_DRIVERS_INCLUDE_SYSTIMER_H_ */
