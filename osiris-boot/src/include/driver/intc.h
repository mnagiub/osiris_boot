/*
 * intc.h
 *
 *  Created on: Jan 13, 2022
 *      Author: mena
 */

#ifndef SRC_DRIVERS_INCLUDE_INTC_H_
#define SRC_DRIVERS_INCLUDE_INTC_H_

#include <configs/config.h>
#include "driver.h"

struct intc_driver {
	struct driver_struct* pvdrv;
	int (*intc_init)(struct intc_driver* pdev);
	int (*intc_deinit)(struct intc_driver* pdev);
	void* pdata;
};

extern int intc_platform_create(struct intc_driver* pdev);
extern int intc_enable_irq(struct intc_driver* pdev, const u32 irqn);
extern int intc_disable_irq(struct intc_driver* pdev, const u32 irqn);
extern int intc_get_irq_status(struct intc_driver* pdev, const u32 irqn, u8 const* status);


#endif /* SRC_DRIVERS_INCLUDE_INTC_H_ */
