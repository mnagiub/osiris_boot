/*
 * linkedlist.h
 *
 *  Created on: Jan 13, 2022
 *      Author: mena
 */

#ifndef SRC_INCLUDE_LINKEDLIST_H_
#define SRC_INCLUDE_LINKEDLIST_H_

#include <configs/config.h>

struct list_head {
	struct list_head* next;
	struct list_head* prev;
};

extern int linkedlist_initialize(struct list_head* head);
extern int linkedlist_push(struct list_head* elm, struct list_head* head);
extern int linkedlist_enqueue(struct list_head* elm, struct list_head* head);
extern int linkedlist_remove(struct list_head* elm);

#define list_for_each_entry(iter, head, member) \
	for(iter = head; (NULL != head) && (NULL != iter) && (NULL != head->next) && (iter != head); iter = iter->next)


#endif /* SRC_INCLUDE_LINKEDLIST_H_ */
