/*
 * error.h
 *
 *  Created on: Jan 13, 2022
 *      Author: mena
 */

#ifndef SRC_INCLUDE_ERROR_H_
#define SRC_INCLUDE_ERROR_H_

#define EOK 		0
#define ENOK		1
#define EINVARG		2
#define EINVOP		3




#endif /* SRC_INCLUDE_ERROR_H_ */
