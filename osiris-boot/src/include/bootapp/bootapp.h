/*
 * bootapp.h
 *
 *  Created on: Jan 22, 2022
 *      Author: mena
 */

#ifndef SRC_INCLUDE_BOOTAPP_BOOTAPP_H_
#define SRC_INCLUDE_BOOTAPP_BOOTAPP_H_

#include <configs/config.h>

#ifdef CONFIG_BOOT_APP
#if (CONFIG_BOOT_APP == 'Y')

extern int bootapp_main(int argc, char* argv[]);

#endif
#endif

#endif /* SRC_INCLUDE_BOOTAPP_BOOTAPP_H_ */
