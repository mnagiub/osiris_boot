/*
 * arch.h
 *
 *  Created on: Jan 13, 2022
 *      Author: mena
 */

#ifndef SRC_INCLUDE_ARCH_H_
#define SRC_INCLUDE_ARCH_H_

#include <configs/config.h>

extern int cpu_init(const u32 mode, const u32 flags);
extern int mem_init(const u32 mode, const u32 flags);
extern int soc_init(const u32 mode, const u32 flags);

extern int cpu_wait_for_interrupt(u32 const* flags);

extern int preempt_disable(void);
extern int preempt_enable(void);
extern int preempt_get_status(u8 const* status);

extern int print_cpu_context(const u8 cpuid, char* buf, u16 const* len);


#endif /* SRC_INCLUDE_ARCH_H_ */
