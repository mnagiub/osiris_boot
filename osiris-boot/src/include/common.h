/*
 * common.h
 *
 *  Created on: Jan 14, 2022
 *      Author: mena
 */

#ifndef SRC_INCLUDE_COMMON_H_
#define SRC_INCLUDE_COMMON_H_

#define offsetof(st, m) \
    ((size_t)&(((st *)0)->m))

#define container_of(ptr, type, member) ({                      \
        const typeof( ((type *)0)->member ) *__mptr = (ptr);    \
        (type *)( (char *)__mptr - offsetof(type,member) );})

#endif /* SRC_INCLUDE_COMMON_H_ */
