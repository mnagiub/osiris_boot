/*
 * delay.h
 *
 *  Created on: Jan 14, 2022
 *      Author: mena
 */

#ifndef SRC_INCLUDE_BOOT_SLEEP_H_
#define SRC_INCLUDE_BOOT_SLEEP_H_

#include <configs/config.h>

#ifdef CONFIG_SLEEP_FUNCS
#if (CONFIG_SLEEP_FUNCS == 'Y')

extern int delay_calibrate(u32 calibration_period_ms);

#ifdef CONFIG_SEC_SLEEP
#if (CONFIG_SEC_SLEEP == 'Y')
extern int delay_sec(u32 sec);
#endif
#endif

#ifdef CONFIG_MS_SLEEP
#if (CONFIG_MS_SLEEP == 'Y')
extern int delay_msec(u32 msec);
#endif
#endif

#ifdef CONFIG_US_SLEEP
#if (CONFIG_US_SLEEP == 'Y')
extern int delay_usec(u32 usec);
#endif
#endif

extern int sleep_for_interrupt(u32 const* flags);

#endif
#endif

#endif /* SRC_INCLUDE_BOOT_SLEEP_H_ */
