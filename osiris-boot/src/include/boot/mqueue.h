/*
 * message.h
 *
 *  Created on: Jan 14, 2022
 *      Author: mena
 */

#ifndef SRC_INCLUDE_BOOT_MQUEUE_H_
#define SRC_INCLUDE_BOOT_MQUEUE_H_

#include <configs/config.h>

#ifdef CONFIG_MESSAGE_QUEUE
#if (CONFIG_MESSAGE_QUEUE == 'Y')
extern int msgq_init(void);
extern int msgq_push_msg(const u8 mqid, const u8* const buf, size_t len);
extern int msgq_pop_msg(const u8 mqid, u8* const buf, size_t len);
extern int msgq_enqueue_msg(const u8 mqid, const u8* const buf, size_t len);
extern int msgq_dequeue_msg(const u8 mqid, u8* const buf, size_t len);
#endif
#endif

#endif /* SRC_INCLUDE_BOOT_MQUEUE_H_ */
