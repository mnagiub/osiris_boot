/*
 * event.h
 *
 *  Created on: Jan 14, 2022
 *      Author: mena
 */

#ifndef SRC_INCLUDE_BOOT_WQUEUE_H_
#define SRC_INCLUDE_BOOT_WQUEUE_H_

#include <configs/config.h>

#ifdef CONFIG_WAIT_QUEUE
#if (CONFIG_WAIT_QUEUE == 'Y')
extern int waitq_init(void);
extern int waitq_wait_for_event(const u8 wqid, u64* const evt_flags, const u8 clear);
extern int waitq_wait_for_event_in_irq(const u8 wqid, u64* const evt_flags, const u8 clear);
extern int waitq_send_event(const u8 wqid, const u64 evt_flags);
extern int waitq_send_event_from_irq(const u8 wqid, const u64 evt_flags);
#endif
#endif

#endif /* SRC_INCLUDE_BOOT_WQUEUE_H_ */
