/*
 * spinlock.h
 *
 *  Created on: Jan 14, 2022
 *      Author: mena
 */

#ifndef SRC_INCLUDE_BOOT_MUTEX_H_
#define SRC_INCLUDE_BOOT_MUTEX_H_

#include <configs/config.h>

#ifdef CONFIG_MUTEX
#if (CONFIG_MUTEX == 'Y')

extern int mutex_init(void);
extern int mutex_acquire(const u8 mtxid);
extern int mutex_release(const u8 mtxid);
extern int mutex_acquire_nonsleep(const u8 mtxid);
extern int mutex_try_acquire(const u8 mtxid);
extern int mutex_release_from_irq(const u8 mtxid);

#endif
#endif

#endif /* SRC_INCLUDE_BOOT_MUTEX_H_ */
