/*
 * stdlib.h
 *
 *  Created on: Jan 14, 2022
 *      Author: mena
 */

#ifndef SRC_INCLUDE_LIB_STDLIB_H_
#define SRC_INCLUDE_LIB_STDLIB_H_

#include <configs/config.h>

#ifndef MACH_NATIVE_LINUX

#ifndef NULL
#define NULL (void*)0
#endif

typedef unsigned int size_t;

extern void* malloc (size_t size);
extern void free (void* ptr);
extern int atoi (const char * str);
extern char *  itoa ( int value, char * str, int base );
#else

#include <stdlib.h>

#endif

#endif /* SRC_INCLUDE_LIB_STDLIB_H_ */
