/*
 * string.h
 *
 *  Created on: Jan 13, 2022
 *      Author: mena
 */

#ifndef SRC_INCLUDE_STRING_H_
#define SRC_INCLUDE_STRING_H_

#include <configs/config.h>

#ifndef MACH_NATIVE_LINUX

extern size_t strlen ( const char * str );
extern char * strcat ( char * destination, const char * source );
extern char * strcpy ( char * destination, const char * source );
extern int strcmp ( const char * str1, const char * str2 );
extern void * memset ( void * ptr, int value, size_t num );
extern void * memcpy ( void * destination, const void * source, size_t num );
extern int memcmp ( const void * ptr1, const void * ptr2, size_t num );
#else

#include <string.h>

#endif

#endif /* SRC_INCLUDE_STRING_H_ */
