/*
 * stdio.h
 *
 *  Created on: Jan 14, 2022
 *      Author: mena
 */

#ifndef SRC_INCLUDE_LIB_STDIO_H_
#define SRC_INCLUDE_LIB_STDIO_H_

#include <configs/config.h>

#ifndef MACH_NATIVE_LINUX
extern int sprintf(char *str, const char *format, ...);

#else

#include <stdio.h>

#endif

#endif /* SRC_INCLUDE_LIB_STDIO_H_ */
