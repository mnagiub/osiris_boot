/*
 * board.h
 *
 *  Created on: Jan 13, 2022
 *      Author: mena
 */

#ifndef SRC_INCLUDE_BOARD_H_
#define SRC_INCLUDE_BOARD_H_

#include <configs/config.h>

struct board_struct {
	void* pboard;
	int (*board_init)(struct board_struct* pbrd, const u32 mode, const u64 flags);
	int (*board_powerup)(struct board_struct* pbrd, const u32 mode, const u64 flags);
	int (*board_boot)(struct board_struct* pbrd, const u32 mode, const u64 flags);
	int (*board_start)(struct board_struct* pbrd, const u32 mode, const u64 flags);
	int (*board_shutdown)(struct board_struct* pbrd, const u32 mode, const u64 flags);
	int (*board_deinit)(struct board_struct* pbrd);
	void* pdata;
};

extern int platform_initialize(void);

#ifdef ENABLE_GPIO /* enable GPIO port */
extern int board_get_gpio_device_count(struct board_struct* dev, u8 const* cnt);
extern void* board_get_gpio_device(struct board_struct* dev, const u8 hndl);
#endif

#ifdef ENABLE_INTC /* enable interrupt controller */
extern int board_get_intc_device_count(struct board_struct* dev, u8 const* cnt);
extern void* board_get_intc_device(struct board_struct* dev, const u8 hndl);
#endif

#ifdef ENABLE_SERIAL /* enable serial port */
extern int board_get_serial_device_count(struct board_struct* dev, u8 const* cnt);
extern void* board_get_serial_device(struct board_struct* dev, const u8 hndl);
#endif

#ifdef ENABLE_SYSTIMER /* enable system timer */
extern int board_get_systimer_device_count(struct board_struct* dev, u8 const* cnt);
extern void* board_get_systimer_device(struct board_struct* dev, const u8 hndl);
#endif

#endif /* SRC_INCLUDE_BOARD_H_ */
