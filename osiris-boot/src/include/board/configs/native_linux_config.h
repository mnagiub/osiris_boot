/*
 * linux_native_config.h
 *
 *  Created on: Jan 13, 2022
 *      Author: mena
 */

#ifndef SRC_CONFIGS_LINUX_NATIVE_CONFIG_H_
#define SRC_CONFIGS_LINUX_NATIVE_CONFIG_H_

#define ARCH native
#define SOC native
#define MACH_NATIVE_LINUX 'Y'
#define ARCH_NATIVE_LINUX 'Y'
#define SOC_NATIVE_LINUX 'Y'
#define MACHINE_INTC_SUPPORTED 'N'
#define MACHINE_SERIAL_SUPPORTED 'Y'
#define COMPATIBLE_WITH_LINUX_CONSOLE 'Y'
#define MACHINE_SYSTIMER_SUPPORTED 'Y'
#define COMPATIBLE_WITH_LINUX_TIMERS 'Y'

#define MAX_DEVICE_COUNT 2
#define HEAP_MAX_SIZE_KB	512

#endif /* SRC_CONFIGS_LINUX_NATIVE_CONFIG_H_ */
