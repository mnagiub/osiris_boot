/*
 * osiris_config.h
 *
 *  Created on: Jan 14, 2022
 *      Author: mena
 */

#ifndef SRC_CONFIGS_OSIRIS_CONFIG_H_
#define SRC_CONFIGS_OSIRIS_CONFIG_H_

#define CONFIG_BOOT_DELAY					'Y'
#ifdef CONFIG_BOOT_DELAY
#if (CONFIG_BOOT_DELAY == 'Y')
#define BOOT_DELAY_SEC						2
#endif
#endif

#define CONFIG_WAIT_QUEUE					'N'
#ifdef CONFIG_WAIT_QUEUE
#if (CONFIG_WAIT_QUEUE == 'Y')
#define CONFIG_WAIT_QUEUE_CNT				4
#define CONFIG_WAIT_QUEUE_LEN				8
#endif
#endif

#define CONFIG_MESSAGE_QUEUE				'N'
#ifdef CONFIG_MESSAGE_QUEUE
#if (CONFIG_MESSAGE_QUEUE == 'Y')
#define CONFIG_MESSAGE_QUEUE_CNT			4
#define CONFIG_MESSAGE_QUEUE_LEN			8
#define CONFIG_MESSAGE_SIZE					16
#endif
#endif

#define CONFIG_MUTEX						'N'
#ifdef CONFIG_MUTEX
#if (CONFIG_MUTEX == 'Y')
#define CONFIG_MUTEX_CNT					8
#endif
#endif

#define CONFIG_SLEEP_FUNCS					'N'
#ifdef CONFIG_SLEEP_FUNCS
#if (CONFIG_SLEEP_FUNCS == 'Y')
#define CONFIG_SEC_SLEEP					'Y'
#define CONFIG_MS_SLEEP						'Y'
#define CONFIG_US_SLEEP						'Y'
#define DELAY_CALIB_DURATION_MS				250
#endif
#endif

#define CONFIG_DISPLAY_BANNER				'N'

#define CONFIG_BOOT_APP						'N'

#endif /* SRC_CONFIGS_OSIRIS_CONFIG_H_ */
