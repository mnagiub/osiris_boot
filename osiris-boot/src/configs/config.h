/*
 * config.h
 *
 *  Created on: Jan 13, 2022
 *      Author: mena
 */

#ifndef SRC_CONFIGS_CONFIG_H_
#define SRC_CONFIGS_CONFIG_H_

#define CONFIG_FILENAME _config.h>
#define BOARD_CONFIG_ <include/board/configs/ MACHINE_NAME
#define BOARD_CONFIG_H BOARD_CONFIG_ CONFIG_FILENAME

#include BOARD_CONFIG_H

#ifndef ARCH
#error "Unknown architecture, define valid machine name"
#endif

#ifndef SOC
#error "Unknown SOC, define valid machine name"
#endif

#define TYPES_FILENAME /include/types.h>
#define TYPES_ <arch/ ARCH
#define TYPES_H TYPES_ TYPES_FILENAME

#define BOARD_INTERFACES_FILENAME /board_interfaces.h>
#define BACKSLASH /
#define BOARD_INTERFACES <board/ SOC
#define BOARD_INTERFACES_ BOARD_INTERFACES BACKSLASH
#define BOARD_INTERFACES_MACHNAME_ BOARD_INTERFACES_ MACHINE_NAME
#define BOARD_INTERFACES_MACHNAME_H BOARD_INTERFACES_MACHNAME_ BOARD_INTERFACES_FILENAME

#ifdef MACHINE_GPIO_SUPPORTED
#if (MACHINE_GPIO_SUPPORTED == 'Y') /* enable gpio port */
#define ENABLE_GPIO
#endif
#endif

#ifdef MACHINE_INTC_SUPPORTED
#if (MACHINE_INTC_SUPPORTED == 'Y') /* enable interrupt controller */
#define ENABLE_INTC
#endif
#endif

#ifdef MACHINE_SERIAL_SUPPORTED
#if (MACHINE_SERIAL_SUPPORTED == 'Y') /* enable serial port */
#define ENABLE_SERIAL
#endif
#endif

#ifdef MACHINE_SYSTIMER_SUPPORTED
#if (MACHINE_SYSTIMER_SUPPORTED == 'Y') /* enable system timer */
#define ENABLE_SYSTIMER
#endif
#endif

#include TYPES_H
#include <configs/osiris_config.h>

#endif /* SRC_CONFIGS_CONFIG_H_ */
