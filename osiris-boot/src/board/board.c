/*
 * boad_framework.c
 *
 *  Created on: Jan 13, 2022
 *      Author: mena
 */

#include <configs/config.h>
#include <include/compiler/stdlib.h>
#include <include/board/board.h>

#ifdef MACH_NATIVE_LINUX
#if (MACH_NATIVE_LINUX == 'Y')
#include "native_linux_board_interfaces.h"
#endif
#endif

struct board_struct board;

extern int board_initialize(struct board_struct* pbrd);

#ifdef ENABLE_GPIO /* enable GPIO port */
int board_get_gpio_device_count(struct board_struct* dev, u8 const* cnt) {

}

void* board_get_gpio_device(struct board_struct* dev, const u8 hndl) {

}

#endif

#ifdef ENABLE_INTC /* enable interrupt controller */
int board_get_intc_device_count(struct board_struct* dev, u8 const* cnt) {

}

void* board_get_intc_device(struct board_struct* dev, const u8 hndl) {

}
#endif

#ifdef ENABLE_SERIAL /* enable serial port */
int board_get_serial_device_count(struct board_struct* dev, u8 const* cnt) {
	return 0;
}

void* board_get_serial_device(struct board_struct* dev, const u8 hndl) {
	return NULL;
}
#endif

#ifdef ENABLE_SYSTIMER /* enable system timer */
int board_get_systimer_device_count(struct board_struct* dev, u8 const* cnt) {
	return 0;
}

void* board_get_systimer_device(struct board_struct* dev, const u8 hndl) {
	return NULL;
}
#endif

int platform_initialize(void) {
	return board_initialize(&board);
}
