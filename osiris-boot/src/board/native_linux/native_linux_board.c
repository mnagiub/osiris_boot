/*
 * native_linux_board.c
 *
 *  Created on: Jan 13, 2022
 *      Author: mena
 */

#include <configs/config.h>
#include <include/board/board.h>
#include <include/common.h>
#include <include/error.h>
#include <include/arch/arch.h>
#include <include/compiler/stdlib.h>
#include <include/driver/serial.h>
#include <include/driver/systimer.h>

#if defined (MACH_NATIVE_LINUX)
#if 'Y' == MACH_NATIVE_LINUX

struct native_linux_board_devices {
	char* board_name;
	struct serial_driver console;
	struct systimer_driver timer;
};

extern int board_initialize(struct board_struct* pbrd);

static int board_init(struct board_struct* pbrd, const u32 mode, const u64 flags) {
	return EOK;
}

static int board_powerup(struct board_struct* pbrd, const u32 mode, const u64 flags) {
	return EOK;
}

static int board_boot(struct board_struct* pbrd, const u32 mode, const u64 flags) {
	return EOK;
}

static int board_start(struct board_struct* pbrd, const u32 mode, const u64 flags) {
	return EOK;
}

static int board_shutdown(struct board_struct* pbrd, const u32 mode, const u64 flags) {
	return EOK;
}

static int board_deinit(struct board_struct* pbrd) {
	return EOK;
}

static struct native_linux_board_devices board_devices;

static struct board_struct native_linux_board = {
	.board_init = board_init,
	.board_powerup = board_powerup,
	.board_boot = board_boot,
	.board_start = board_start,
	.board_start = board_start,
	.board_shutdown = board_shutdown,
	.board_deinit = board_deinit,
	.pdata = &board_devices
};

int board_initialize(struct board_struct* pbrd) {

	if (NULL == pbrd) return ENOK;

	pbrd = &native_linux_board;
	native_linux_board.pboard = &native_linux_board;

#if defined (MACHINE_SERIAL_SUPPORTED) && (MACHINE_SERIAL_SUPPORTED == 'Y')
	serial_platform_create(1);
	serial_device_create(&board_devices.console);
#endif

#if defined (MACHINE_SYSTIMER_SUPPORTED) && (MACHINE_SYSTIMER_SUPPORTED == 'Y')
	systimer_platform_create(1);
	systimer_device_create(&board_devices.timer);
#endif

	return EOK;
}

#endif
#endif
