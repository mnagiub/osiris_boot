/*
 * linux_serial.c
 *
 *  Created on: Jan 23, 2022
 *      Author: mena
 */

#include <configs/config.h>
#include <include/common.h>
#include <include/error.h>
#include <include/driver/driver.h>
#include <include/driver/serial.h>
#include <include/arch/arch.h>

#ifdef COMPATIBLE_WITH_LINUX_CONSOLE
#if COMPATIBLE_WITH_LINUX_CONSOLE == 'Y'

#include <drivers/serial/linux_console_interfaces.h>

/* Linux header files based on gcc */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

struct console_priv_data {
	u8 baudrate;
	u8 n_stopbit : 2;
	u8 n_startbit : 2;
	u8 n_baud_bits : 4;
	u8 parity : 2;
	u8 tx_enabled : 1;
	u8 rx_enabled : 1;
	u8 en_hw_flowctrl : 1;
	u8 opened : 1;
};

static int console_init (struct serial_driver* pdev) {
	if (NULL == pdev) return ENOK;

	pdev->pdata = malloc(sizeof(struct console_priv_data));
	if (NULL == pdev->pdata) return ENOK;

	((struct console_priv_data*)(pdev->pdata))->baudrate = LINUX_CONSOLE_BAUDRATE_115220;
	((struct console_priv_data*)(pdev->pdata))->n_stopbit = LINUX_CONSOLE_N_STOPBITS_0;
	((struct console_priv_data*)(pdev->pdata))->n_startbit = LINUX_CONSOLE_N_STARTBITS_0;
	((struct console_priv_data*)(pdev->pdata))->parity = LINUX_CONSOLE_NO_PARITY;
	((struct console_priv_data*)(pdev->pdata))->n_baud_bits = LINUX_CONSOLE_N_BAUDBITS_8;
	((struct console_priv_data*)(pdev->pdata))->en_hw_flowctrl = LINUX_CONSOLE_HW_FLOWCTRL_DIS;

	((struct console_priv_data*)(pdev->pdata))->tx_enabled = 0;
	((struct console_priv_data*)(pdev->pdata))->rx_enabled = 0;
	((struct console_priv_data*)(pdev->pdata))->opened = 0;

	return EOK;
}

static int console_open (struct serial_driver* pdev, const u32 mode, const u64 flags) {

	if (NULL == pdev) return ENOK;
	if (NULL == pdev->pdata) return ENOK;

	if (((struct console_priv_data*)(pdev->pdata))->opened == 0)
		((struct console_priv_data*)(pdev->pdata))->opened = 1;
	else
		return EINVOP;

	return EOK;
}

static int console_read (struct serial_driver* pdev, char* const buf, size_t* const len) {

	if (NULL == pdev) return ENOK;
	if (NULL == pdev->pdata) return ENOK;
	if (NULL == buf) return ENOK;
	if (NULL == len) return ENOK;

	if (((struct console_priv_data*)(pdev->pdata))->rx_enabled == 1) {
		if (fgets(buf, (*len), stdin)) {
			buf[strcspn(buf, "\n")] = '\0';
		}
		(*len) = strlen(buf);
	}

	return EOK;
}

static int console_write (struct serial_driver* pdev, const char* const buf, const size_t len) {

	if (NULL == pdev) return ENOK;
	if (NULL == pdev->pdata) return ENOK;

	if (((struct console_priv_data*)(pdev->pdata))->opened == 0) return EINVOP;

	if (((struct console_priv_data*)(pdev->pdata))->tx_enabled == 1) {
		fprintf(stdout, "\n%s", buf);
	}

	return EOK;
}

static int console_ioctl (struct serial_driver* pdev, const u32 mode, const u64 flags, u64* const pdata) {

	if (NULL == pdev) return ENOK;
	if (NULL == pdev->pdata) return ENOK;

	switch (mode) {
	case LINUX_CONSOLE_IOCTL_EN_TX:
		((struct console_priv_data*)(pdev->pdata))->tx_enabled = 1;
		break;

	case LINUX_CONSOLE_IOCTL_EN_RX:
		((struct console_priv_data*)(pdev->pdata))->rx_enabled = 1;
		break;

	case LINUX_CONSOLE_IOCTL_DIS_TX:
		((struct console_priv_data*)(pdev->pdata))->tx_enabled = 0;
		break;

	case LINUX_CONSOLE_IOCTL_DIS_RX:
		((struct console_priv_data*)(pdev->pdata))->rx_enabled = 0;
		break;

	case LINUX_CONSOLE_IOCTL_WRITE_CHAR:
		if (NULL == pdata) return ENOK;
		putc((*((char*)pdata)), stdout);
		break;

	case LINUX_CONSOLE_IOCTL_READ_CHAR:
		if (NULL == pdata) return ENOK;
		(*((char*)pdata)) = getc(stdin);
		break;

	default:
		return EINVOP;
	}

	return EOK;
}

static int console_poll (struct serial_driver* pdev, const u32 timeout) {
	if (NULL == pdev) return ENOK;
	if (NULL == pdev->pdata) return ENOK;

	return EOK;

}

static int console_close (struct serial_driver* pdev) {

	if (NULL == pdev) return ENOK;
	if (NULL == pdev->pdata) return ENOK;

	if (((struct console_priv_data*)(pdev->pdata))->opened == 1)
		((struct console_priv_data*)(pdev->pdata))->opened = 0;
	else
		return EINVOP;

	return EOK;
}

static int console_deinit (struct serial_driver* pdev) {
	if (NULL == pdev) return ENOK;
	if (NULL == pdev->pdata) return ENOK;

	return EOK;
}

int linux_serial_console_create(struct serial_driver* pdev, const u8 chndl) {

	if (NULL == pdev) return EINVARG;

	if (driver_register_device(&pdev->pvdrv, chndl)) return ENOK;

	pdev->serial_close = console_close;
	pdev->serial_deinit = console_deinit;
	pdev->serial_init = console_init;
	pdev->serial_ioctl = console_ioctl;
	pdev->serial_open = console_open;
	pdev->serial_poll = console_poll;
	pdev->serial_read = console_read;
	pdev->serial_write = console_write;

	return pdev->serial_init(pdev);
}

#endif
#endif


