/*
 * serial_framework.c
 *
 *  Created on: Jan 13, 2022
 *      Author: mena
 */


#include <configs/config.h>
#include <include/common.h>
#include <include/error.h>
#include <include/compiler/stdlib.h>
#include <include/driver/driver.h>
#include <include/driver/serial.h>

#ifdef COMPATIBLE_WITH_LINUX_CONSOLE
#if COMPATIBLE_WITH_LINUX_CONSOLE == 'Y'
#include "linux_console_interfaces.h"
#endif
#endif

static u8 chndl;

int serial_platform_create(const u8 dev_cnt) {

	if ((dev_cnt < 1) || (dev_cnt > MAX_DEVICE_COUNT)) return EINVARG;

	if (!driver_register_device_class("serial", dev_cnt, &chndl)) return ENOK;

	return EOK;
}

int serial_device_create(struct serial_driver* pdev) {

	int ret = 0;

#ifdef COMPATIBLE_WITH_LINUX_CONSOLE
#if COMPATIBLE_WITH_LINUX_CONSOLE == 'Y'
	ret = linux_serial_console_create(pdev, chndl);
#endif
#endif

	return ret;
}

int serial_open(const char* const dev_name, struct serial_driver** ppdev) {

	struct driver_struct* pdev;

	if (driver_open_device(dev_name, &pdev)) return ENOK;

	(*ppdev) = container_of(pdev, struct serial_driver, pvdrv);

	return EOK;
}

int serial_read(struct serial_driver* pdev, char* const buf, size_t* const len) {

	if (NULL == pdev) return EINVARG;
	if (NULL == pdev->pvdrv) return EINVOP;

	return pdev->serial_read(pdev, buf, len);
}

int serial_write(struct serial_driver* pdev, const char* const buf, const size_t len) {

	if (NULL == pdev) return EINVARG;
	if (NULL == pdev->pvdrv) return EINVOP;

	return pdev->serial_write(pdev, buf, len);
}

int serial_ioctl(struct serial_driver* pdev, const u32 mode, const u64 flags, u64* const pdata) {

	if (NULL == pdev) return EINVARG;
	if (NULL == pdev->pvdrv) return EINVOP;

	return pdev->serial_ioctl(pdev, mode, flags, pdata);
}

int serial_poll(struct serial_driver* pdev, const u32 timeout) {

	if (NULL == pdev) return EINVARG;
	if (NULL == pdev->pvdrv) return EINVOP;

	return pdev->serial_poll(pdev, timeout);
}

int serial_close(struct serial_driver* pdev) {

	if (NULL == pdev) return EINVARG;
	if (NULL == pdev->pvdrv) return EINVOP;

	return pdev->serial_close(pdev);
}
