/*
 * systimer_framework.c
 *
 *  Created on: Jan 13, 2022
 *      Author: mena
 */

#include <configs/config.h>
#include <include/driver/driver.h>
#include <include/driver/systimer.h>
#include <include/error.h>
#include <include/common.h>
#include <include/compiler/stdlib.h>

#ifdef COMPATIBLE_WITH_LINUX_TIMERS
#if COMPATIBLE_WITH_LINUX_TIMERS == 'Y'
#include "linux_systimer_interfaces.h"
#endif
#endif

static u8 chndl;

int systimer_platform_create(const u8 dev_cnt) {

	if ((dev_cnt < 1) || (dev_cnt > MAX_DEVICE_COUNT)) return EINVARG;

	if (!driver_register_device_class("systimer", dev_cnt, &chndl)) return ENOK;

	return EOK;
}

int systimer_device_create(struct systimer_driver* pdev) {

	int ret = 0;

#ifdef COMPATIBLE_WITH_LINUX_TIMERS
#if COMPATIBLE_WITH_LINUX_TIMERS == 'Y'
	ret = linux_systimer_create(pdev, chndl);
#endif
#endif

	return ret;
}

int systimer_open(const char* const dev_name, struct systimer_driver** ppdev) {

	struct driver_struct* pdev;

	if (driver_open_device(dev_name, &pdev)) return ENOK;

	(*ppdev) = container_of(pdev, struct systimer_driver, pvdrv);

	return EOK;
}

int systimer_start_timer(struct systimer_driver* pdev, const struct systimer_conf_struct conf, const u32 mode) {

	if (NULL == pdev) return EINVARG;
	if (NULL == pdev->pvdrv) return EINVOP;

	preempt_disable();

	pdev->callback = conf.callback;
	pdev->callback_data = conf.callback_data;
	pdev->period_us = conf.period_us;
	pdev->s_interval_us = conf.s_interval_us;

	preempt_enable();

	pdev->systimer_ioctl(pdev, SYSTIMER_IOCTL_TIMER_MODE, 0, NULL);
	return pdev->systimer_ioctl(pdev, SYSTIMER_IOCTL_TIMER_START, 0, NULL);
}

int systimer_stop_timer(struct systimer_driver* pdev) {

	if (NULL == pdev) return EINVARG;
	if (NULL == pdev->pvdrv) return EINVOP;

	return pdev->systimer_ioctl(pdev, SYSTIMER_IOCTL_TIMER_STOP, 0, NULL);
}

int systimer_start_frc(struct systimer_driver* pdev, const u32 mode, const u64 flags) {

	if (NULL == pdev) return EINVARG;
	if (NULL == pdev->pvdrv) return EINVOP;

	pdev->systimer_ioctl(pdev, SYSTIMER_IOCTL_FRC_MODE, 0, NULL);
	return pdev->systimer_ioctl(pdev, SYSTIMER_IOCTL_FRC_START, 0, NULL);
}

int systimer_stop_frc(struct systimer_driver* pdev) {

	if (NULL == pdev) return EINVARG;
	if (NULL == pdev->pvdrv) return EINVOP;

	pdev->systimer_ioctl(pdev, SYSTIMER_IOCTL_FRC_MODE, 0, NULL);
	return pdev->systimer_ioctl(pdev, SYSTIMER_IOCTL_FRC_STOP, 0, NULL);
}

int systimer_read_frc(struct systimer_driver* pdev, u64* const val) {

	if (NULL == pdev) return EINVARG;
	if (NULL == pdev->pvdrv) return EINVOP;

	return pdev->systimer_read(pdev, val);
}

int systimer_close(struct systimer_driver* pdev) {

	return EOK;
}
