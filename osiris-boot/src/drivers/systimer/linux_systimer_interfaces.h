/*
 * linux_systimer_interfaces.h
 *
 *  Created on: Jan 22, 2022
 *      Author: mena
 */

#ifndef SRC_DRIVERS_SYSTIMER_LINUX_SYSTIMER_INTERFACES_H_
#define SRC_DRIVERS_SYSTIMER_LINUX_SYSTIMER_INTERFACES_H_

#include <configs/config.h>
#include <include/driver/driver.h>
#include <include/driver/systimer.h>

#ifdef COMPATIBLE_WITH_LINUX_TIMERS
#if COMPATIBLE_WITH_LINUX_TIMERS == 'Y'

extern int linux_systimer_create(struct systimer_driver* pdev, const u8 chndl);

#endif
#endif

#endif /* SRC_DRIVERS_SYSTIMER_LINUX_SYSTIMER_INTERFACES_H_ */
