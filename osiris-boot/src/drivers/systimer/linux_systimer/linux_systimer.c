/*
 * linux_systimer.c
 *
 *  Created on: Jan 23, 2022
 *      Author: mena
 */

#include <configs/config.h>
#include <include/common.h>
#include <include/error.h>
#include <include/driver/driver.h>
#include <include/driver/systimer.h>
#include <include/arch/arch.h>

#ifdef COMPATIBLE_WITH_LINUX_TIMERS
#if COMPATIBLE_WITH_LINUX_TIMERS == 'Y'

#define TIMER_FREQ_100_MS	100000000

#include <drivers/systimer/linux_systimer_interfaces.h>

/* Linux header files based on gcc */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <signal.h>
#include <time.h>

#define TIMER_MODE	1
#define FRC_MODE	2
#define NO_MODE		0

struct timer_priv_data {
	u8 frc_started : 1;
	u8 timer_started : 1;
	u8 timer_mode : 2;
	u64 ticks_cnt;
};

struct timers_list_node {
	struct systimer_driver* pdev;
	struct timers_list_node* next;
};

static u8 created_timer_handler = 0;

static struct timers_list_node* timers_list = NULL;

static void central_timer_handler (int sig, siginfo_t *si, void *uc) {

	struct timers_list_node* head;

	for (head = timers_list; (NULL != head) && (NULL != head->next); head = head->next) {

		if (((struct timer_priv_data*)head->pdev->pdata)->timer_started)
			if (NULL != head->pdev->callback)
				head->pdev->callback(head->pdev->callback_data);

		((struct timer_priv_data*)head->pdev->pdata)->ticks_cnt++;
	}

	printf("\n Signal received");
    /* signal(sig, SIG_IGN); */
}

static int timer_init (struct systimer_driver* pdev) {

	struct timers_list_node* head;

	pdev->pdata = malloc(sizeof(struct timer_priv_data));
	if (NULL == pdev->pdata) return ENOK;

	((struct timer_priv_data*)pdev->pdata)->frc_started = 0;
	((struct timer_priv_data*)pdev->pdata)->timer_started = 0;
	((struct timer_priv_data*)pdev->pdata)->timer_mode = NO_MODE;
	((struct timer_priv_data*)pdev->pdata)->ticks_cnt = 0;

	if (!created_timer_handler) {

	    timer_t timerid;
	    struct sigevent sev;
	    struct itimerspec its;
	    long long freq_nanosecs;
	    sigset_t mask;
	    struct sigaction sa;

	    /* Establish handler for timer signal. */
	    sa.sa_flags = SA_SIGINFO;
	    sa.sa_sigaction = central_timer_handler;
	    sigemptyset(&sa.sa_mask);
	    if (sigaction(SIGRTMIN, &sa, NULL) == -1) return ENOK;

	    /* Block timer signal temporarily. */
	    sigemptyset(&mask);
	    sigaddset(&mask, SIGRTMIN);
	    if (sigprocmask(SIG_SETMASK, &mask, NULL) == -1) return ENOK;

	    /* Create the timer. */
	    sev.sigev_notify = SIGEV_SIGNAL;
	    sev.sigev_signo = SIGRTMIN;
	    sev.sigev_value.sival_ptr = &timerid;
	    if (timer_create(CLOCK_REALTIME, &sev, &timerid) == -1) return ENOK;

	    /* Start the timer. */
	    freq_nanosecs = TIMER_FREQ_100_MS;
	    its.it_value.tv_sec = freq_nanosecs / 1000000000;
	    its.it_value.tv_nsec = freq_nanosecs % 1000000000;
	    its.it_interval.tv_sec = its.it_value.tv_sec;
	    its.it_interval.tv_nsec = its.it_value.tv_nsec;

	    if (timer_settime(timerid, 0, &its, NULL) == -1) return ENOK;

	    /* Unlock the timer signal, so that timer notification
	       can be delivered. */
	    if (sigprocmask(SIG_UNBLOCK, &mask, NULL) == -1) return ENOK;

	    created_timer_handler = 1;
	}

	head = timers_list;
	while ((NULL != head) && (NULL != head->next))
		head = head->next;

	if (NULL == head) {
		timers_list = malloc(sizeof(struct timers_list_node));

		if (NULL == timers_list) return ENOK;

		timers_list->next = NULL;
		timers_list->pdev = pdev;

	} else {
		head->next = malloc(sizeof(struct timers_list_node));

		if (NULL == head->next) return ENOK;

		head = head->next;

		head->next = NULL;
		head->pdev = pdev;
	}

	return EOK;
}

static int timer_deinit (struct systimer_driver* pdev) {
	return EINVOP;
}

static int timer_ioctl (struct systimer_driver* pdev, const u32 mode, const u64 flags, u64* const pdata) {

	if (NULL == pdev) return ENOK;
	if (NULL == pdev->pdata) return ENOK;

	switch (mode) {
	case SYSTIMER_IOCTL_TIMER_MODE:
		((struct timer_priv_data*)(pdev->pdata))->timer_mode = SYSTIMER_IOCTL_TIMER_MODE;
		break;

	case SYSTIMER_IOCTL_FRC_MODE:
		((struct timer_priv_data*)(pdev->pdata))->timer_mode = SYSTIMER_IOCTL_FRC_MODE;
		break;

	case SYSTIMER_IOCTL_TIMER_START:
		if (((struct timer_priv_data*)(pdev->pdata))->timer_mode == SYSTIMER_IOCTL_TIMER_MODE)
			((struct timer_priv_data*)(pdev->pdata))->timer_started = 1;
		break;

	case SYSTIMER_IOCTL_TIMER_STOP:
		if (((struct timer_priv_data*)(pdev->pdata))->timer_mode == SYSTIMER_IOCTL_TIMER_MODE) {
			((struct timer_priv_data*)(pdev->pdata))->timer_started = 0;
			((struct timer_priv_data*)(pdev->pdata))->ticks_cnt = 0;
		}
		break;

	case SYSTIMER_IOCTL_FRC_START:
		if (((struct timer_priv_data*)(pdev->pdata))->timer_mode == SYSTIMER_IOCTL_FRC_MODE)
			((struct timer_priv_data*)(pdev->pdata))->frc_started = 1;
		break;

	case SYSTIMER_IOCTL_FRC_STOP:
		if (((struct timer_priv_data*)(pdev->pdata))->timer_mode == SYSTIMER_IOCTL_FRC_MODE)
			((struct timer_priv_data*)(pdev->pdata))->frc_started = 0;
		break;

	default:
		return EINVOP;
	}

	return EOK;
}

static int timer_read (struct systimer_driver* pdev, u64* const pdata) {

	if (NULL == pdev) return ENOK;
	if (NULL == pdev->pdata) return ENOK;
	if (NULL == pdata) return ENOK;

	if (((struct timer_priv_data*)(pdev->pdata))->timer_mode == SYSTIMER_IOCTL_TIMER_MODE) {
		if (((struct timer_priv_data*)(pdev->pdata))->timer_started == 1) {
			(*pdata) = ((struct timer_priv_data*)(pdev->pdata))->ticks_cnt;
		} else {
			(*pdata) = 0;
		}
	} else if (((struct timer_priv_data*)(pdev->pdata))->timer_mode == SYSTIMER_IOCTL_FRC_MODE) {
		if (((struct timer_priv_data*)(pdev->pdata))->frc_started == 1) {
			struct timespec ts;
			clock_gettime(CLOCK_REALTIME, &ts);
			(*pdata) = ts.tv_nsec;
		} else {
			(*pdata) = 0;
		}
	} else {
		(*pdata) = 0;
	}

	return EOK;
}

int linux_systimer_create(struct systimer_driver* pdev, const u8 chndl) {

	if (NULL == pdev) return EINVARG;

	if (driver_register_device(&pdev->pvdrv, chndl)) return ENOK;

	pdev->systimer_init = timer_init;
	pdev->systimer_deinit = timer_deinit;
	pdev->systimer_ioctl = timer_ioctl;
	pdev->systimer_read = timer_read;

	return pdev->systimer_init(pdev);
}

#endif
#endif
