/*
 * driver.c
 *
 *  Created on: Jan 22, 2022
 *      Author: mena
 */

#include <configs/config.h>
#include <include/common.h>
#include <include/error.h>
#include <include/compiler/stdlib.h>
#include <include/compiler/string.h>
#include <include/driver/driver.h>
#include <include/arch/arch.h>

#define MAX_CLASSNAME_LEN	32
#define MAX_DEVICENAME_LEN	48

struct class_private_data {
	char* cls_name;
	u8 curr_dev_cnt;
	u8 curr_active_dev_cnt;
	u16 cls_dev_table_enrty;
};

struct driver_private_data {
	u8 status;
};

static char classname_buf[MAX_DEVICE_COUNT][MAX_CLASSNAME_LEN];
static char devicename_buf[MAX_DEVICE_COUNT][MAX_DEVICENAME_LEN];
static char workbuff [MAX_DEVICENAME_LEN];

static int hndl_cnt = 0;
static int cls_cnt = 0;
static int devhndl_cnt = 0;

#ifndef MAX_DEVICE_COUNT
#error "Define MAX_DEVICE_COUNT in board configuration file to define maximum number of devices in system"
#endif

static struct driver_struct device_table [MAX_DEVICE_COUNT] = {0};
static struct driver_private_data device_priv_table [MAX_DEVICE_COUNT] = {0};
static struct class_private_data class_priv_table [MAX_DEVICE_COUNT] = {0};

int driver_register_device_class(const char* const cls_name, const u8 dev_cnt, u8* const pchndl) {

	if (NULL == cls_name) return ENOK;
	if (0 == strlen(cls_name)) return ENOK;
	if (NULL == pchndl) return ENOK;

	if (cls_cnt >= MAX_DEVICE_COUNT) return ENOK;

	if ((dev_cnt + hndl_cnt) > MAX_DEVICE_COUNT) return ENOK;

	memset(classname_buf[cls_cnt], 0, MAX_CLASSNAME_LEN);

	preempt_disable();

	class_priv_table[cls_cnt].cls_name = classname_buf[cls_cnt];
	class_priv_table[cls_cnt].curr_dev_cnt = dev_cnt;
	class_priv_table[cls_cnt].curr_active_dev_cnt = 0;
	class_priv_table[cls_cnt].cls_dev_table_enrty = devhndl_cnt;

	strcpy(class_priv_table[cls_cnt].cls_name, cls_name);

	(*pchndl) = cls_cnt;
	cls_cnt++;

	devhndl_cnt = class_priv_table[cls_cnt].cls_dev_table_enrty + dev_cnt;

	preempt_enable();


	return EOK;
}

int driver_register_device(struct driver_struct** pdev, const u8 chndl) {

	u16 devhndl = 0;

	if (NULL == pdev) return ENOK;
	if (chndl > cls_cnt) return ENOK;

	preempt_disable();

	if (class_priv_table[chndl].curr_active_dev_cnt >= class_priv_table[chndl].curr_dev_cnt) {

		preempt_enable();
		return ENOK;
	}

	devhndl = class_priv_table[chndl].cls_dev_table_enrty + class_priv_table[chndl].curr_active_dev_cnt;

	device_table[devhndl].cls_name = class_priv_table[chndl].cls_name;
	memset(devicename_buf[devhndl], 0, MAX_DEVICENAME_LEN);
	memset(workbuff, 0, MAX_DEVICENAME_LEN);
	strcpy(devicename_buf[devhndl], device_table[devhndl].cls_name);
	sprintf (workbuff, "%d", class_priv_table[chndl].curr_active_dev_cnt);
	strcat(devicename_buf[devhndl], workbuff);
	device_table[devhndl].dev_name = devicename_buf[devhndl];
	device_table[devhndl].class_id = chndl;
	device_table[devhndl].devhndl = devhndl;
	device_table[devhndl].dev_num = class_priv_table[chndl].curr_active_dev_cnt;
	device_priv_table[devhndl].status = 1;


	(*pdev) = &device_table[devhndl];

	preempt_enable();

	return EOK;

}

int driver_unregister_device(struct driver_struct* const pdev) {

	if (NULL == pdev) return ENOK;
	if (pdev->devhndl >= MAX_DEVICE_COUNT) return ENOK;

	preempt_disable();
	if (device_priv_table[pdev->devhndl].status == 0) {
		preempt_enable();
		return EINVOP;
	}

	device_priv_table[pdev->devhndl].status = 0;

	preempt_enable();

	return EOK;
}

int driver_unregister_device_class(const char* const cls_name) {
	return EINVOP;
}

int driver_open_device(const char* const dev_name, struct driver_struct** ppdev) {

	for (int i = 0; i < MAX_DEVICE_COUNT; i++) {
		if (!strcmp(dev_name, device_table[i].dev_name)) {
			(*ppdev) = &device_table[i];
			return EOK;
		}
	}

	return ENOK;
}
