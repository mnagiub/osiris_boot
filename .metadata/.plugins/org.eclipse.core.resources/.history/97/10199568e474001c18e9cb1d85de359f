/*
 * timer.h
 *
 *  Created on: Jan 13, 2022
 *      Author: mena
 */

#ifndef SRC_DRIVERS_INCLUDE_SYSTIMER_H_
#define SRC_DRIVERS_INCLUDE_SYSTIMER_H_

struct systimer_driver {
	void* pdevice;
	int (*systimer_init)(struct systimer_driver* pdev);
	int (*systimer_deinit)(struct systimer_driver* pdev);
	void* pdata;
};

#ifndef SYS_CLK_FREQ_HZ
#define SYS_CLK_FREQ_HZ	100
#endif

#define SYSTIMER_MS_TO_HZ(ms)	((ms * SYS_CLK_FREQ_HZ) / 1000)
#define SYSTIMER_US_TO_HZ(us)	((us * SYS_CLK_FREQ_HZ) / 1000000)
#define SYSTIMER_SEC_TO_HZ(sec)	(sec * SYS_CLK_FREQ_HZ)


extern int systimer_platform_create(struct systimer_driver* pdev);
extern int systimer_start_timer(struct systimer_driver* pdev, const u32 mode,
		const u64 flags, const u32 s_interval,
		const u32 period, int (*callback)(struct systimer_driver* pdev, void* data), void* data);

extern int systimer_stop_timer(struct systimer_driver* dev);

extern int systimer_start_frc(struct systimer_driver* pdev, const u32 mode, const u64 flags);
extern int systimer_stop_frc(struct systimer_driver* pdev);

#endif /* SRC_DRIVERS_INCLUDE_SYSTIMER_H_ */
