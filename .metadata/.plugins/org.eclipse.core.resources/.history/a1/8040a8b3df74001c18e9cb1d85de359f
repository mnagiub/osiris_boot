/*
 * serial.h
 *
 *  Created on: Jan 13, 2022
 *      Author: mena
 */

#ifndef SRC_DRIVERS_INCLUDE_SERIAL_H_
#define SRC_DRIVERS_INCLUDE_SERIAL_H_

#include <configs/config.h>

struct serial_driver {
	void* pdevice;
	int (*serial_init)(struct serial_driver* pdev, const u32 mode, const u64 flags);
	int (*serial_open)(struct serial_driver* pdev, const u32 mode, const u64 flags);
	int (*serial_read)(struct serial_driver* pdev, char const* buf, u8 const* len);
	int (*serial_write)(struct serial_driver* pdev, const char const* buf, const u8 len);
	int (*serial_ioctl)(struct serial_driver* pdev, const u32 mode, const u64 flags);
	int (*serial_poll)(struct serial_driver* pdev, const u32 mode, const u64 flags);
	int (*serial_close)(struct serial_driver* pdev);
	int (*serial_deinit)(struct serial_driver* pdev);
	void* pdata;
};

extern int serial_platform_create(struct serial_driver* pdev);
extern struct serial_driver* serial_open(const char const* devname);
extern int serial_enable_rx(struct serial_driver* pdev);
extern int serial_disable_rx(struct serial_driver* pdev);
extern int serial_transmit(struct serial_driver* pdev, const char const* buf, const u8 len);
extern int serial_read_data(struct serial_driver* pdev, char const* buf, u8 const* len);
extern int serial_send_char(struct serial_driver* pdev, const char c);
extern int serial_read_char(struct serial_driver* pdev, char const* c);
extern int serial_poll(struct serial_driver* pdev, const u32 timeout);
extern int serial_close(struct serial_driver* pdev);

#endif /* SRC_DRIVERS_INCLUDE_SERIAL_H_ */
